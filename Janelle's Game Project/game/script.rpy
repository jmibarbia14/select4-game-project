﻿# The script of the game goes in this file.

# Declare characters.

define narrator = Character(what_italic=True)
define mc = Character("[name]", color="#E75480")
define ai = Character("IRIS", color="#CE1620")
define boss = Character("Olivia Cebba", color="#6082B6")
define prof = Character("Zeki Montgomery", color="#B57EDC")
define gold = Character("...", color="#FFD700")
define n = Character("News", color="#8DB600")

# Define transitions.

define dissolve = Dissolve(1.0)

# Define constant dialoguesself.
# Problem with the usage...

$ death = "{i}You died.{/i}"
$ tbc = "{i}To be continued...{/i}"

$ killer = "{color=#FFD700}The Yellow Rose Killer{/color}"

# The game starts here.

label start:

    # Intro.

    scene bg fitting room
    with dissolve

    # Ask the player for a name to use for the main character.

    "Oh! Hello there!"

    "My name is Janelle and I am the developer of this game."

    "What is your {color=#E75480}name{/color}?"

    python:
        name = renpy.input("{color=#E75480}Type{/color} your name below.\n"
        + "Or press {color=#E75480}ENTER{/color} to use the default name {i}Taylor{/i}.\n")

        name = name.strip() or "Taylor"

    "It's nice to meet you, [mc]!"

    "Welcome to my game!"

    "Before we officially start, I just have a quick reminder."

    "The flow of the game depends entirely on your choices."

    "So choose wisely..."

    # Official start of the gameplay.

    scene bg train inside
    with dissolve

    "You are snapped out of your daydream by the familiar topic on the radio."

    n "...It has been 2 months since the last known attack by {color=#FFD700}The Yellow Rose Killer{/color}."

    n "And yet, the police are still baffled on why did these events happen and who committed the crime."

    n "As you might know, {color=#FFD700}The Yellow Rose Killer{/color} have committed over 14 murders..."

    "Again, you are snapped out of your focus by the vibration on your wrist."

    "You study the watch-like device in your hand."

    "Your AI {color=#CE1620}[ai]{/color} smiles at the screen. You tap the screen to let her speak."

    ai "Hello, [mc]."

    "Her voice is soothing in your ears."

    ai "The temperature this afternoon is 30 degrees Farenheit. I hope you're wearing your warm jacket."

    ai "Don't forget to meet up with {color=#6082B6}Ms. Cebba{/color}. She's expecting you at her home."

    "A few minutes later, you arrive at the station."

    scene bg train outside
    with dissolve

    "You stepped out of the train car."

    "You adjust your jacket. Iris was right. You should've worn the right one."

    mc "Sigh.{w} Too late to worry about that now."

    "[boss]'s house is not too far away from the train station so you walk your way towards it."

    "Halfway there, [ai] vibrated yet again."

    ai "You just received a message from {color=#B57EDC}Professor Montgomery{/color}. {nw}"
    extend "He is insisting that you see him right away."

    "You know his house is in the next town."

    menu:
    mc "(Where do I go?)"

        "Continue your way to Olivia's.":
            jump choice1_olivia

        "Visit Zeki first.":
            jump choice1_zeki

    label choice1_olivia:

        mc "Iris, please tell Zeki that I have to meet my boss first."

        ai "But you know how paranoid Professor-"

        mc "Olivia is my boss. Zeki will have to wait."

        ai "Okay, [mc]. Would you like me to guide you to Ms. Cebba's house?"

        mc "No thanks, Iris. I got this."

        ai "No problem. You know where I am."

        scene bg house one
        with dissolve

        "You arrived at her house. You looked at the house. It looks nice for such an odd lady."

        "You rang the doorbell."

        "Olivia opened the door. She is a tall woman with pale skin and shoulder-length dark hair. {nw}"
        extend "Her other features is difficult to determine because of the dim light, {nw}"
        extend "but you know her body language suggests that she is tired."

        menu:
        mc "(What do I say?)"

            "Is there anything wrong?":
                jump choice2_concern

            "What do you need me for?":
                jump choice2_straight

        label choice2_concern:

            boss "Nothing. Thank you for the concern, but you don't need to worry about me."

            "You've known her for so long. You know she wouldn't let anyone know about her emotions."

            "You notice the folder in her hands."

            mc "You got trouble getting that?"

            boss "There never was. We're professionals."

            "There's a little hint of annoyance in her voice."

            jump choice2_straight

    label choice2_straight:

        boss "Here is everything that you need for the next trip."

        "She handed you the folder. You looked inside. Case file, IDs, credit card, and itinerary. The usual stuff."

        ai "Scanning itinerary...{w} Travel destination: {color=#E75480}Mystique{/color}."

        mc "Mystique? {color=#FFD700}The location of the first killing?{/color}"

        "You couldn't determine her expression, but you could tell she wasn't thrilled."

        boss "Yeah. Rest up. You'll need it."

        "She closes the door. Hard."

        "You remember your heated conversation yesterday."

        "She doesn't agree that you're taking the case.{w} Not her."

        ai "[mc], are we heading to Mystique now?"

        mc "Not yet. We still need to find out what Zeki has to say."

        "You started making my way towards the Professor's house."

        "To be continued..."

        return

    label choice1_zeki:

        mc "Iris, please tell Olivia that I am sorry that I might be late."

        ai "Are you sure, [mc]? You know she won't like it if you turn up late-"

        mc "Zeki's information might be equally important, Iris. And you know him.{nw}"
        extend "If I don't show up, he'll be paranoid."

        ai "Okay, [mc]. Would you like me to guide you to Professor Montgomery's house?"

        mc "No thanks, Iris. I got this."

        ai "No problem. You know where I am."

        "You call a cab and head to Zeki's house."

        scene bg house two
        with dissolve

        "You arrived at his house. You looked at his house and wondered why he lived in such a huge house."

        "You were just walking on his front lawn when you hear a muffled noise coming from the inside."

        "You pushed the buzzer and waited."

        "A voice came through the intercom."

        prof "Yes? Who's there?"

        "The voice sounded a little strange. More than the usual, anyway."

        mc "It's just me, Zeki."

        prof "Oh, good! I'm in the basement."

        "There was a relief in his voice."

        "CLICK!{w} The door opened and I let myself in."

        "The first thing that you noticed is the large suitcase by the couch.{nw}"
        extend" You had the impression that it was overpacked because of the bulging sides."

        mc "Zeki never mentioned going on a trip, right Iris?"

        ai "Yes, [mc]. He never mentioned any trips."

        menu:
        mc "(What do I do?)"

            "Examine the suitcase.":
                jump choice3_snoop

            "Leave it alone":
                jump choice3_ignore

            label choice3_snoop:

                "You knelt by the luggage and examined it."

                "There was a tag labeled {color=#E75480}Domus{/color}."

                mc "Domus? Why is he going there?"

                ai "I have no idea, [mc]. Also, he never mentioned this place before."

                "The luggage looked like it was packed in a hurry.{nw} "
                extend "He wasn't a neat person, but you know this was messy by for his standards."

                jump choice3_done

            label choice3_ignore:

                mc "I can't snoop around. I don't want to add up to his paranoia."

                jump choice3_done

        label choice3_done:

            "You head to the basement. It was always pitch black down here."

            "You feel your way down to where the light switch would be. Before you could flip it-"

            prof "No! It must remain dark!"

            mc "Why can't I turn on the lights?"

            prof "Don't ask foolish questions! Come here! Quick!"

            "A candle is lit, highlighting the professor's unshaven face.{nw}"
            extend " His reddened eyes gaze at me as he beckoned me by his bony hand."

            "That's when you saw out of the corner of your eye: a man standing at the top of the staircase."

            gold "Tch."

            return
